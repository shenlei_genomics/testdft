#pragma  once
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include "Tools.h"
#include <numeric>
#include <math.h>
#include <algorithm>
#include <iterator>
#include <stdlib.h>    
#include <stdio.h> 
#include <thread>
#include <time.h>  

using namespace cv;
using namespace std;

Mat rho;
int low_freq_beg = 10, high_freq_beg = 150;
int TH = 40000;

struct ImgClarity
{
	string filename;
	vector<float> corner_ratio;
	//float min_ratio;
	ImgClarity()
	{
		filename = "";
		//min_ratio = 0.0f;
	}
	ImgClarity(const string& fn)
	{
		filename = fn;
		//min_ratio = 0.0f;
	}
	~ImgClarity()
	{
		corner_ratio.clear();
	}
	void operator=(const ImgClarity& ic)
	{
		filename = ic.filename;
		corner_ratio = ic.corner_ratio;
	}
};
static void help(char* progName)
{
	cout << endl
		<< "This program output sorted images info acc. to their focus clarity. " << endl
		<< "The dft of an image is taken and it's power spectrum is used to calcualte its clarity." << endl
		<< "Usage:" << endl
		<< progName << " imgsPath outputCsvFn " << endl;
        //<< progName << " imgsPath outputCsvFn [-s]"                       << endl
		//<< "-s is optional, if input -s, will print all of the power spectrum info.";
}

int genCornerImgs(Mat& I, const int& rows, const int& cols, vector<Mat>& corners)
{
	if (I.empty() || I.rows < rows || I.cols < cols)
	{
		return -1;
	}
	for (size_t y = 0; y < I.rows; ++y)
	{
		for (size_t x = 0; x < I.cols; ++x)
		{
			//cout << I.at<unsigned short>(y, x) << endl;
			if (I.at<unsigned short>(y,x) > TH)
			{
				I.at<unsigned short>(y, x) = 0;
			}
		}
	}
	Mat tl = I(Range(0, rows), Range(0,cols));
	Mat tr = I(Range(0,rows), Range(I.cols - cols, I.cols));
	Mat dl = I(Range(I.rows - rows, I.rows), Range(0, cols));
	Mat dr = I(Range(I.rows - rows, I.rows), Range(I.cols - cols, I.cols));	
	Point start = Point((I.cols >> 1) - (cols >> 1), (I.rows >> 1) - (rows >> 1));
	Mat center = I(Range(start.y, start.y + rows), Range(start.x, start.x + cols));
	corners.clear();
	corners.push_back(tl);
	corners.push_back(tr);
	corners.push_back(dr);
	corners.push_back(dl);
	corners.push_back(center);
	//imwrite("./center.tif", center);
}

void genPolar(const int cy, const int cx, Mat& mag)
{
	Mat x = Mat::zeros(cy * 2, cx * 2, CV_32F);
	Mat y = Mat::zeros(cy * 2, cx * 2, CV_32F);
	for (int r = -cy; r < cy; ++r)
	{
		for (int c = -cx; c < cx; ++c)
		{
			x.at<float>(r + cy, c + cx) = static_cast<float>(c);
			y.at<float>(r + cy, c + cx) = static_cast<float>(r);
		}
	}
	Mat angle;
	cartToPolar(x, y, mag, angle);	
}

int writeFreq(const string& filename, const vector<float>& freq);
void findLocalPeak(const vector<float>& src, const int sz, vector<size_t>& pk_index);
void findMaxInPeaks(const vector<float>& freq_spec, const vector<size_t>& pk_index, float& max_val, size_t& max_index, float& second_max_val, size_t& second_max_index);
void getFreqSpectrum(const Mat& rho, const Mat& power, vector<float>& freq_spectrum)
{
	Scalar s = sum(power);
	float power_spt_sum = s(0);
	freq_spectrum.clear();
	freq_spectrum.assign(ceil(sqrt(power.rows * power.rows + power.cols*power.cols) / 2), 0.0f);
	for (size_t y = 0; y < power.rows; ++y)
	{
		for (size_t x = 0; x < power.cols; x++)
		{
 			size_t index = static_cast<size_t>(floor(rho.at<float>(y, x)));
			//cout << "index = " << index << "\tx = " << x << "\ty = " << y << "\trho = " << rho.at<float>(y,x) << "\n";
			freq_spectrum[index] += power.at<float>(y, x);
		}
	}
	for (vector<float>::iterator it = freq_spectrum.begin(); it != freq_spectrum.end(); ++it)
	{
		*it /= power_spt_sum;
	}
}

void findMaxInPeaks(const vector<float>& freq_spec, const vector<size_t>& pk_index, float& max_val, float& second_max_val);

int powerSpectrumD(const Mat& I, const int& high_freq_beg, const string& img_fn, vector<float>& ratio)//vector<float>& freq_spectrum)
{	
	if (I.empty())
		return -1;

	Mat padded;                            //expand input image to optimal size
	int m = getOptimalDFTSize(I.rows);
	int n = getOptimalDFTSize(I.cols); // on the border add zero values
	copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));	
	Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
	Mat complexI,powerSpt;
	merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

	dft(complexI, complexI);            // this way the result may fit in the source matrix

	// compute the magnitude and switch to logarithmic scale
	// => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
	split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
	magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
	Mat magI;
	magI = planes[0];
	powerSpt = magI.mul(magI);

	// crop the spectrum, if it has an odd number of rows or columns
	powerSpt = powerSpt(Rect(0, 0, powerSpt.cols & -2, powerSpt.rows & -2));

	// rearrange the quadrants of Fourier image  so that the origin is at the image center
	int cx = powerSpt.cols / 2;
	int cy = powerSpt.rows / 2;

	Mat q0(powerSpt, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
	Mat q1(powerSpt, Rect(cx, 0, cx, cy));  // Top-Right
	Mat q2(powerSpt, Rect(0, cy, cx, cy));  // Bottom-Left
	Mat q3(powerSpt, Rect(cx, cy, cx, cy)); // Bottom-Right

	Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);
	
	vector<float> freq_spectrum;
	getFreqSpectrum(rho, powerSpt, freq_spectrum);
#define WRITE_FREQ_SPETRUM
#ifdef WRITE_FREQ_SPETRUM	
	ofstream fs("./freq_spectrum.txt", ios::app);
	if (fs)
	{
		fs << img_fn << "\t";
		copy(begin(freq_spectrum)+10, end(freq_spectrum), std::ostream_iterator<float>(fs, "\t"));
		fs << "\n";
	}
	fs.close();
#endif
	if (high_freq_beg + 21 > freq_spectrum.size())
	{
		cerr << "high freq start from:" << high_freq_beg << ", it's out of range!" << endl;
		return -1;
	}
	//vector<float> high_freq_spectrum(freq_spectrum.begin() + high_freq_beg, freq_spectrum.end());
	//vector<size_t> pk_index;
	//findLocalPeak(high_freq_spectrum, 10, pk_index);
	//if (pk_index.size() < 1)
	//{
	//	ratio.push_back(0.0f);
	//	return 0;
	//}
	//if (pk_index.size() < 2)
	//{
	//	//ratio.push_back(freq_spectrum[pk_index[0]] * (high_freq_beg - low_freq_beg) / accumulate(freq_spectrum.cbegin() + low_freq_beg, freq_spectrum.cbegin() + high_freq_beg, 0.0f));
	//	ratio.push_back(freq_spectrum[pk_index[0]]  / freq_spectrum[pk_index[0]-10]);
	//	return 0;
	//}
	//float max_val, second_max_val;
	//size_t max_index, second_max_index;
	//findMaxInPeaks(high_freq_spectrum, pk_index, max_val, max_index, second_max_val, second_max_index);
	//max_index += high_freq_beg;
	//second_max_index += high_freq_beg;
	//size_t start, end;
	//max_index > second_max_index ? (start = second_max_index + 11, end = max_index - 10) : (start = max_index + 11, end = second_max_index - 10);
	//float avg = 0.0f;
	//if (end <= start)
	//{
	//	freq_spectrum.push_back(2.0f);
	//}
	//else
	//{
	//	for (size_t i = start; i < end; i++)
	//	{
	//		avg += freq_spectrum[i];
	//	}
	//	avg /= (end - start);
	//	ratio.push_back((max_val + second_max_val) / avg);
	//}
	////ratio.push_back((max_val + second_max_val)*(high_freq_beg - low_freq_beg) / accumulate(freq_spectrum.cbegin() + low_freq_beg, freq_spectrum.cbegin() + high_freq_beg, 0.0f));
	//writeFreq("E:/data/V0.2test/freq.txt", freq_spectrum);
	//vector<float> high_freq_spectrum(freq_spectrum.begin() + high_freq_beg, freq_spectrum.end());
	//vector<size_t> pk_index;
	//findLocalPeak(high_freq_spectrum, 10, pk_index);
	//if (pk_index.size() < 1)
	//{
	//	ratio.push_back(0.0f);
	//	return 0;
	//}
	//if (pk_index.size() < 2)
	//{
	//	//ratio.push_back(freq_spectrum[pk_index[0]] * (high_freq_beg - low_freq_beg) / accumulate(freq_spectrum.cbegin() + low_freq_beg, freq_spectrum.cbegin() + high_freq_beg, 0.0f));
	//	ratio.push_back(freq_spectrum[pk_index[0]]  / freq_spectrum[pk_index[0]-10]);
	//	return 0;
	//}

	/*writeFreq("./freq_blur_4_1_C.txt", freq_spectrum);
	vector<float> high_freq_spectrum(freq_spectrum.begin() + high_freq_beg, freq_spectrum.end());
	vector<size_t> pk_index;
	findLocalPeak(high_freq_spectrum, 10, pk_index);
	if (pk_index.size() < 1)
	{
		ratio.push_back(0.0f);
		return 0;
	}
	float max_val, second_max_val;
	size_t max_index, second_max_index;
	findMaxInPeaks(high_freq_spectrum, pk_index, max_val, max_index, second_max_val, second_max_index);	
	if (pk_index.size() < 2)
	{		
		max_index += high_freq_beg;
		ratio.push_back(freq_spectrum[pk_index[0]] * (max_index - 10 - low_freq_beg) / accumulate(freq_spectrum.cbegin() + low_freq_beg, freq_spectrum.cbegin() + max_index - 10, 0.0f));
		return 0;
	}
	size_t end = max_index < second_max_index ? max_index - 10 + high_freq_beg : second_max_index - 10 + high_freq_beg;	
	ratio.push_back((max_val + second_max_val)*(end - low_freq_beg) / accumulate(freq_spectrum.cbegin() + low_freq_beg, freq_spectrum.cbegin() + end, 0.0f));*/
	
	vector<float> high_freq_spectrum(freq_spectrum.begin() + high_freq_beg, freq_spectrum.end());
	vector<size_t> pk_index;
	findLocalPeak(high_freq_spectrum, 10, pk_index);
	if (pk_index.size() < 1)
	{
		ratio.push_back(0.0f);
		return 0;
	}
	float low_avg = accumulate(freq_spectrum.cbegin() + low_freq_beg, freq_spectrum.cbegin() + high_freq_beg, 0.0f) / (high_freq_beg - low_freq_beg);
	if (pk_index.size() < 2)
	{
		ratio.push_back(high_freq_spectrum[pk_index[0]]);
		return 0;
	}
	float max_val, second_max_val;
	findMaxInPeaks(high_freq_spectrum, pk_index, max_val, second_max_val);
	ratio.push_back((max_val + second_max_val) / low_avg);

	return 0;
}

int writeResult(const string& filename, const vector<ImgClarity>& imgs_clarity);


/*@belief: init global params includes bright contaminant Threshold, low_freq_beg, high_freq_beg, rho
@param   I   pick-up the first img as input to calculate these params
*/
void initGlobalParams(const Mat& I)
{
	int m = getOptimalDFTSize(I.rows >> 2);
	int n = getOptimalDFTSize(I.cols >> 2); // on the border add zero values
	int row = m >> 1, col = n >> 1;
	genPolar(row, col, rho);
	int rad = sqrtf(row*row + col*col);
	high_freq_beg = rad / 3;
	low_freq_beg = rad / 6;
	vector<short> intensity;
	for (size_t y = 0; y < I.rows; y++)
	{
		for (size_t x = 0; x < I.cols; x++)
		{
			intensity.push_back(I.at<short>(y, x));
		}
	}
	nth_element(begin(intensity), begin(intensity) + 0.9995*intensity.size(), end(intensity));
	TH = intensity[0.9995*intensity.size()];
	std::cout << "low_freqs begins at:" << low_freq_beg << std::endl;
	std::cout << "high_freqs begins at:" << high_freq_beg << std::endl;
	std::cout << "TH: " << TH << "\tIntensity brighter than "<< TH << " is treated as bright spot or contaminants." << std::endl;
}

template<typename Iterator, typename Iterator_result>
struct calFreq_block
{
	void operator()(Iterator first, Iterator last, Iterator_result result)
	{
		for (auto itf = first; itf != last; ++itf)
		{
			//cout << "Processing image: " << *itf << endl;
			Mat I = imread(*itf, CV_LOAD_IMAGE_ANYDEPTH);
			vector<Mat> corners;
			if (genCornerImgs(I, I.rows >> 2, I.cols >> 2, corners) < 0)
			{
				++result;
				continue;
			}
			ImgClarity ic(*itf);
			int index = 0;
			for (vector<Mat>::const_iterator itc = corners.cbegin(); itc != corners.cend(); ++itc)
			{
				char drive[_MAX_DRIVE];
				char dir[_MAX_DIR];
				char fname[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath(itf->c_str(), drive, dir, fname, ext);
				sprintf(fname, "%s_%d", fname, index);
				powerSpectrumD(*itc, high_freq_beg, string(fname), ic.corner_ratio);
				index++;
			}
			//ic.min_ratio = *min_element(ic.corner_ratio.cbegin(), ic.corner_ratio.cend());
			*result = ic;
			++result;
		}
	}
};

template<typename Iterator>
int parallel_calFreq_block(Iterator first, Iterator last, vector<ImgClarity>& result)
{
	unsigned long const length = std::distance(first, last);

	if (!length)
		return -2;

	unsigned long const min_per_thread = 50;
	unsigned long const max_threads = (length + min_per_thread - 1) / min_per_thread;

	unsigned long const hardware_threads = std::thread::hardware_concurrency();
	 
	unsigned long const num_threads = std::min(hardware_threads != 0 ? hardware_threads : 2, max_threads);
	unsigned long const block_size = length / num_threads;

	vector<std::thread> threads(num_threads - 1);
	Iterator block_start = first;
	vector<ImgClarity>::iterator result_start = std::begin(result);
	for (unsigned long i = 0; i < (num_threads - 1); ++i)
	{
		Iterator block_end = block_start;
		std::advance(block_end, block_size);
		threads[i] = std::thread(calFreq_block<Iterator, vector<ImgClarity>::iterator>(),block_start, block_end, result_start);
		block_start = block_end;
		std::advance(result_start, block_size);
	}
	calFreq_block<Iterator, vector<ImgClarity>::iterator>()(block_start, last, result_start);
	std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));

	return 0;
}

int main(int argc, char ** argv)
{   
	if (argc < 3)
	{
		help(argv[0]);
		return -1;
	}
	const string dir = argv[1] ;
	const string outputFn = argv[2];

	vector<string> files;
	if (getFiles(dir, ".tif", files) < 0)
	{
		cerr << "input images path " << argv[1] << " error!" << endl;
		return -1;
	}

	Mat I = imread(files[0], CV_LOAD_IMAGE_ANYDEPTH);
	if (I.empty())
	{
		cerr << "Read img " << files[0] << " error!" << endl;
		return -1;
	}
	initGlobalParams(I);
	I.release();

	vector<ImgClarity> imgs_clarity(files.size());
	for (size_t i = 0; i < files.size(); i++)
	{
		imgs_clarity[i].filename = files[i];
		imgs_clarity[i].corner_ratio={ 0.0f,0.0f,0.0f,0.0f,0.0f };
		//imgs_clarity[i].min_ratio = 0.0f;
	}
	cout << "Begin running------------------------------------------" << endl;
	//long beginTime = clock();//获得开始时间，单位为毫秒  
	parallel_calFreq_block(std::begin(files), std::end(files), imgs_clarity);
	//long endTime = clock();//获得结束时间  
	//cout << "Running Time:" << (endTime - beginTime)/1000 <<"s"<< endl;
	cout << "Done!" << endl;
	
	//sort(imgs_clarity.begin(), imgs_clarity.end(), [](const ImgClarity& ic1, const ImgClarity& ic2)->bool {return ic1.filename < ic2.filename; });
	writeResult(outputFn, imgs_clarity);

    return 0;
}




void findLocalPeak(const vector<float>& src, const int sz, vector<size_t>& pk_index)
{
	const int M = src.size();	
	int m = sz;
	while (m < M)
	{
		Point ijmax;
		vector<float> part_vec(src.cbegin() + m-sz , src.cbegin() + min(m + sz + 1,M));
		double lmax;
		minMaxLoc(part_vec, NULL, &lmax, NULL, &ijmax, noArray());		
		if (ijmax.x == sz)
		{
			pk_index.push_back(m);
			m = m + sz + 1;			
		}		
		else
		{
			m = m + 1;
		}
 	}
}

void findMaxInPeaks(const vector<float>& freq_spec, const vector<size_t>& pk_index, float& max_val, float& second_max_val)
{
	vector<float> local_peaks;
	max_val = 0.0f;	
	size_t max_index = -1;
	for (vector<size_t>::const_iterator it = pk_index.cbegin(); it != pk_index.cend(); ++it)
	{
		if (freq_spec[*it] > max_val)
		{
			max_val = freq_spec[*it];
			max_index = *it;
		}
	}
	second_max_val = 0.0f;
	for (vector<size_t>::const_iterator it = pk_index.cbegin(); it != pk_index.cend(); ++it)
	{
		if (*it != max_index)
		{
			if (freq_spec[*it] > second_max_val)
			{
				second_max_val = freq_spec[*it];				
			}
		}
	}
}

void findMaxInPeaks(const vector<float>& freq_spec, const vector<size_t>& pk_index, float& max_val, size_t& max_index, float& second_max_val, size_t& second_max_index)
{
	vector<float> local_peaks;
	max_val = 0.0f;		
	max_index = -1;
	for (vector<size_t>::const_iterator it = pk_index.cbegin(); it != pk_index.cend(); ++it)
	{
		if (freq_spec[*it] > max_val)
		{			
			max_val = freq_spec[*it];
			max_index = *it;
		}
	}
	second_max_val = 0.0f;
	second_max_index = -1;
	for (vector<size_t>::const_iterator it = pk_index.cbegin(); it != pk_index.cend(); ++it)
	{
		if (*it != max_index)
		{
			if (freq_spec[*it] > second_max_val)
			{				
				second_max_val = freq_spec[*it];
				second_max_index = *it;
			}
		}		
	}
}

int writeResult(const string& filename, const vector<ImgClarity>& imgs_clarity)
{
	ofstream fs(filename,'w');
	if (fs.is_open())
	{
		for (vector<ImgClarity>::const_iterator it = imgs_clarity.cbegin(); it != imgs_clarity.cend(); ++it)
		{
			fs << it->filename << ",";
			for (vector<float>::const_iterator itr = it->corner_ratio.cbegin(); itr != it->corner_ratio.cend(); ++itr)
			{
				fs << *itr << ",";
			}
			fs << "\n";
			//fs << it->min_ratio << "\n";
		}
		return 0;
	}
	else
	{
		std::cerr << "open file " << filename << " for writing error!" << std::endl;
		return -1;
	}
}

int writeFreq(const string& filename, const vector<float>& freq)
{
	ofstream fs(filename, ios::app);
	if (fs.is_open())
	{
		for (vector<float>::const_iterator it = freq.cbegin(); it != freq.cend(); ++it)
		{
			fs << *it << "\t";
		}
		fs << "\n";
		return 0;
	}
	else
	{
		std::cerr << "open file " << filename << " for writing error!" << std::endl;
		return -1;
	}
}