#include "Tools.h"

using std::string;
using std::vector;
using cv::Point2f;
using std::ifstream;

int getFiles(const std::string& path, const std::string& ext, std::vector<std::string>& files)
{
	intptr_t file_handle = 0;
	struct _finddata_t file_info;
	std::string path_name, ext_name;
	if (strcmp(ext.c_str(), "") != 0)//if not empty
		ext_name = "\\*" + ext;
	else
		ext_name = "\\*";

	if ((file_handle = _findfirst(path_name.assign(path).append(ext_name).c_str(), &file_info)) != -1)
	{
		do
		{
			if (file_info.attrib & _A_SUBDIR)
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					getFiles(path_name.assign(path).append("\\").append(file_info.name), ext, files);
				}
			}
			else
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					files.push_back(path_name.assign(path).append("\\").append(file_info.name));
					//files.push_back(file_info.name);
				}
			}
		} while (_findnext(file_handle, &file_info) == 0);
		_findclose(file_handle);
	}
	return 0;
}

